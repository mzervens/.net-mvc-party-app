﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace netmd4.Models
{
    public class UserModel
    {
        private string adminUsername = "admin";

        public bool isAdministrator(string login)
        {
            if(login == adminUsername)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        // 0 - login does not exist
        // 1 - login exists
        // 2 - login exists but has no password
        public int checkUserLoginExistance(string login)
        {
            int result = 0;
            if(login == adminUsername)
            {
                result = 1;
            }
            else
            {
                using(netmd4Entities ctx = new netmd4Entities())
                {
                    var res = from g in ctx.guestList
                              where g.email == login
                              select g;
                    List<guestList> guest = res.ToList<guestList>();

                    if(guest.Count != 0)
                    {
                        int guestId = guest[0].id;
                        var hasPass = from u in ctx.users
                                      where u.guestId == guestId
                                      select u;

                        List<users> user = hasPass.ToList<users>();
                        if(user.Count == 0)
                        {
                            result = 2;
                        }
                        else
                        {
                            result = 1;
                        }
                    }
                }

            }


            return result;
        }

        public int login(string login, string password)
        {
            List<users> loginRes = null;
            if(login == adminUsername)
            {
                using(netmd4Entities ctx = new netmd4Entities())
                {
                    var res = from u in ctx.users where u.password == password &&
                                                        u.userAccess == "ADMIN"                      
                              select u;

                    loginRes = res.ToList<users>();
                }
            }
            else
            {
                using(netmd4Entities ctx = new netmd4Entities())
                {
                    var res = from u in ctx.users.Include("guestList").Where(x => x.password == password &&
                                                                                  x.guestList.email == login)
                              select u;

                    loginRes = res.ToList<users>();
                }
            }

            if(loginRes == null || loginRes.Count == 0)
            {
                return -1;
            }
            else
            {
                return loginRes[0].id;
            }
        }

        public int storePassword(int guestId, string password)
        {
            users user = new users();
            user.guestId = guestId;
            user.password = password;
            user.userAccess = "USER";

            using(netmd4Entities ctx = new netmd4Entities())
            {
                ctx.users.Add(user);
                ctx.SaveChanges();

                return user.id;
            }

        }



    }
}