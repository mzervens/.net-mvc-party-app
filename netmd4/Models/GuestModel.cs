﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace netmd4.Models
{
    public class GuestModel
    {
        public int getGuestId(string email)
        { 
            using(netmd4Entities ctx = new netmd4Entities())
            {
                var res = from g in ctx.guestList
                          where g.email == email
                          select g;

                List<guestList> guest = res.ToList<guestList>();
                if(guest.Count == 0)
                {
                    return -1;
                }
                else
                {
                    return guest[0].id;
                }
            }
        }

        public List<guestList> getGuests()
        {
            using(netmd4Entities ctx = new netmd4Entities())
            {
                var res = from g in ctx.guestList.OrderBy(x => x.name + " " + x.surname)
                          select g;

                return res.ToList<guestList>();
            }
        }


        public int createGuest(guestList guest)
        {
            using(netmd4Entities ctx = new netmd4Entities())
            {
                ctx.guestList.Add(guest);
                ctx.SaveChanges();
                return guest.id;
            }

        }


    }
}