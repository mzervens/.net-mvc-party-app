﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace netmd4.Models
{
    public class PresentModel
    {
        public List<presents> getPresents()
        {
            using(netmd4Entities ctx = new netmd4Entities())
            {

                var pres = from p in ctx.presents.OrderBy(x => x.name)
                           select p;

                return pres.ToList<presents>();
            }

        }

        public List<presents> getPresentsTakenBy(int userId)
        {
            using(netmd4Entities ctx = new netmd4Entities())
            {
                var pres = from p in ctx.presents.OrderBy(x => x.name)
                           where p.takenBy == userId
                           select p;

                return pres.ToList<presents>();
            }
        }

        public List<presents> getPresent(int id)
        {
            using(netmd4Entities ctx = new netmd4Entities())
            {
                var pres = from p in ctx.presents
                           where p.id == id
                           select p;

                return pres.ToList<presents>();
            }

        }


        public int saveModifiedPresent(presents p)
        {
            int result = 0;
            using(netmd4Entities ctx = new netmd4Entities())
            {
                ctx.presents.Attach(p);
                ctx.Entry(p).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();

                result = 1;
            }

            return result;
        }

        public int createPresent(presents p)
        {
            int result = 0;
            using(netmd4Entities ctx = new netmd4Entities())
            {
                ctx.presents.Add(p);
                ctx.SaveChanges();
                result = p.id;
            }

            return result;
        }


    }
}