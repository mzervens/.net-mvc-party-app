﻿$(function ()
{
    var changingContent = false;
    var contentShown = 1;

    function refreshElement(elId, element)
    {
        $.ajax({

            url: "/Present/GetPresentMarkup",
            type: "post",
            data: {
                presentId: elId,
                op: "DB"
            },
            success: function(response)
            {
                if(response != 0)
                {
                    element.replaceWith(response);
                }
            }

        });

    }

    function refreshUserPresentList()
    {
        $.ajax({

            url: "/Present/GetRegisteredPresentMarkupTakenBy",
            type: "post",
            data: {

            },
            success: function (response)
            {
                if (response != 0)
                {
                    $("#presentWrapper").replaceWith(response);
                    $("#myPresentFilter").removeClass("btn-default");
                    $("#myPresentFilter").addClass("btn-success");
                }
            }

        });


    }


    $(document).on("click", "#myPresentFilter", function (e)
    {
        $this = $(this);
        if (changingContent)
        {
            return;
        }

        changingContent = true;

        if($this.hasClass("btn-default"))
        {
            $.ajax({

                url: "/Present/GetRegisteredPresentMarkupTakenBy",
                type: "post",
                data: {

                },
                success: function(response)
                {
                    if(response != 0)
                    {
                        $("#presentWrapper").replaceWith(response);
                        $("#myPresentFilter").removeClass("btn-default");
                        $("#myPresentFilter").addClass("btn-success");
                        contentShown = 2;
                    }

                    changingContent = false;
                }

            });

        }
        else
        {
            $.ajax({

                url: "/Present/GetRegisteredPresentMarkup",
                type: "post",
                data: {

                },
                success: function (response)
                {
                    if (response != 0)
                    {
                        $("#presentWrapper").replaceWith(response);
                        $("#myPresentFilter").removeClass("btn-success");
                        $("#myPresentFilter").addClass("btn-default");
                        contentShown = 1;
                    }

                    changingContent = false;
                }

            });
        }


    });

    $(document).on("click", ".check-pres-res", function (e)
    {
        $this = $(this);
        if ($this.hasClass("disabled"))
        {
            return;
        }


        var presentId = $this.attr("data-presentid");

        $.ajax({

            url: "/Present/ChoosePresent",
            type: "post",
            data: {
                presentId: presentId,
                op: "CHECK"
            },
            success: function(response)
            {
                if(response == 1)
                {
                    var element = $this.closest(".col-md-4");
                    refreshElement(presentId, element);
                }
            }
        });

    });


    $(document).on("click", ".uncheck-present-res", function (e)
    {
        $this = $(this);

        var presentId = $this.attr("data-presentid");

        $.ajax({

            url: "/Present/ChoosePresent",
            type: "post",
            data: {
                presentId: presentId,
                op: "UNCHECK"
            },
            success: function (response)
            {
                if (response == 1)
                {
                    var element = $this.closest(".col-md-4");
                    if (contentShown == 2)
                    {
                        refreshUserPresentList();
                    }
                    else
                    {
                        refreshElement(presentId, element);
                    }
                }
            }
        });

    });


});