﻿$(function ()
{
    $(".flake-menu-item").mouseenter(function (e)
    {
        $(this).next(".flake-menu-text").addClass("flake-menu-text-active");
    });

    $(".flake-menu-item").mouseleave(function (e)
    {
        $(this).next(".flake-menu-text").removeClass("flake-menu-text-active");
    });

    $(".flake-menu-item").click(function (e)
    {
        if (!this.hasAttribute("data-click"))
        {
            return;
        }

        window.location = $(this).attr("data-click");

    });

});