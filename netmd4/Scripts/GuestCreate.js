﻿$(function ()
{
    // test email
    // http://stackoverflow.com/questions/2507030/email-validation-using-jquery
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function tryToCreate()
    {
        var guestName = $.trim($("#guestName").val());
        var guestSurname = $.trim($("#guestSurname").val());
        var guestEmail = $.trim($("#guestEmail").val());

        $.ajax({

            url: "/Guest/CreateGuest",
            type: "post",
            data: {
                name: guestName,
                surname: guestSurname,
                email: guestEmail

            },
            success: function(response)
            {
                var result = JSON.parse(response);
                if ("Error" in result)
                {
                    if (result.Error == "Email exists")
                    {
                        $("#guest-create-failed-email").show();
                    }
                    else
                    {
                        $("#guest-create-failed").show();
                    }
                }
                else if("Success" in result)
                {
                    $("#guest-create-success").show();
                    refreshGuestList();
                }
            }

        });

    }

    function clearModalInputs()
    {
        removeErrors();

        $("#guestName").val("");
        $("#guestSurname").val("");
        $("#guestEmail").val("");
    }

    $('#guestCrModal').on('hidden.bs.modal', function ()
    {
        clearModalInputs();
    });

    function refreshGuestList()
    {
        $.ajax({
            url: "/Guest/GetGuestsMarkup",
            type: "post",
            data: {
            },
            success: function(response)
            {
                if(response != 0)
                {
                    $("#guestWrapper").replaceWith(response);
                }
            }

        });


    }

    function removeErrors()
    {
        $(".has-error").removeClass("has-error");
        $(".alert").hide();
    }

    function validateGuest()
    {
        var guestName = $.trim($("#guestName").val());
        var guestSurname = $.trim($("#guestSurname").val());
        var guestEmail = $.trim($("#guestEmail").val());
        var hasError = false;

        if(guestName == "")
        {
            hasError = true;
            $("#guestName").closest("div").addClass("has-error");
        }

        if(guestSurname == "")
        {
            hasError = true;
            $("#guestSurname").closest("div").addClass("has-error");
        }

        if(guestEmail == "" || !isEmail(guestEmail))
        {
            hasError = true;
            $("#guestEmail").closest("div").addClass("has-error");
        }

        return hasError;
    }

    $("#createGuest").click(function (e)
    {
        removeErrors();
        var hasErrors = validateGuest();

        if (!hasErrors)
        {
            tryToCreate();
        }
    });

    $("#clearInputs").click(function (e)
    {
        clearModalInputs();

    });


});
