﻿$(function ()
{
    function swapTexts(element)
    {
        var visibleText = element.html();
        element.html(element.attr("data-secondinfo"));
        element.attr("data-secondinfo", visibleText);

    }


    $(document).on("mouseenter", ".guest-list-item", function (e)
    {
        swapTexts($(this));
    });

    $(document).on("mouseleave", ".guest-list-item", function (e)
    {
        swapTexts($(this));
    });

});