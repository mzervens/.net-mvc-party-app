﻿$(function ()
{
    $("#username").tooltip();
    var userNotRegisteredVisible = false;


    $("input").keypress(function (e)
    {
        if (e.which == 13)
        {
            $("#loginBtn").trigger("click");
        }
    });


    function showUsernameNotRegisteredMsg()
    {
        if (userNotRegisteredVisible)
        {
            return;
        }

        $('#username').tooltip('show');
        userNotRegisteredVisible = true;
    }

    function hideUsernameNotRegisteredMsg()
    {
        if (userNotRegisteredVisible == false)
        {
            return;
        }

        $('#username').tooltip('hide');
        userNotRegisteredVisible = false;
    }

    function checkLoginExistance(login)
    {
        $.ajax({

            url: "/Auth/GetLoginExistance",
            type: "post",
            data: {
                    login: login
            },
            success: function(response)
            {
                handleLoginInput(response);
            }
        });

    }

    function tryLogin(login, pass)
    {
        $.ajax({

            url: "/Auth/TryLogin",
            type: "post",
            data: {
                login: login,
                password: pass
            },
            success: function (response)
            {
                handleLogin(response);
            }
        });

    }

    function handleLogin(response)
    {
        $("#passwordInput").removeClass("has-error");

        if(response == 1)
        {
            window.location = "/App";
        }
        else
        {
            $("#passwordInput").addClass("has-error");
        }

    }

    function tryCreatePass(login, pass, repass) {
        $.ajax({

            url: "/Auth/CreatePassword",
            type: "post",
            data: {
                login: login,
                password: pass,
                rePassword: repass
            },
            success: function (response) {
                handlePasswordCreation(response);
            }
        });

    }

    function handlePasswordCreation(response)
    {
        $("#passwordCreation").removeClass("has-error");
        if (response == 1) {
            window.location = "/App";
        }
        else
        {
            $("#passwordCreation").addClass("has-error");
        }
    }

    function handleLoginInput(response)
    {
        hideUsernameNotRegisteredMsg();

        if(response == 1)
        {
            $("#usernameField").slideUp("fast", function () {
                $(this).hide();
            });

            $("#passwordInput").show();
        }
        else if(response == 2)
        {
            $("#usernameField").slideUp("fast", function () {
                $(this).hide();
            });

            $("#passwordCreation").show();
        }
        else
        {
            setTimeout(function ()
            {
                showUsernameNotRegisteredMsg();
            },
            200);
        }
    }


    $("#loginBtn").on("click", function (e)
    {
        if ($("#usernameField").is(":visible"))
        {
            checkLoginExistance($("#username").val());
        }
        else if($("#passwordInput").is(":visible"))
        {
            tryLogin($("#username").val(), $("#password").val());
        }
        else if($("#passwordCreation").is(":visible"))
        {
            tryCreatePass($("#username").val(), $("#passwordCreate").val(), $("#rePasswordCreate").val());
        }
    });

});