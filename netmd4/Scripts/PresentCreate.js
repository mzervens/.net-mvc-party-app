﻿$(function ()
{
    function validatePresent()
    {
        var name = $.trim($("#presName").val());
        var descr = $.trim($("#presDescr").val());
        var imgUrl = $.trim($("#imgUrl").val());
        var hasErrors = false;

        if(name == "")
        {
            $("#presName").closest("div").addClass("has-error");
            hasErrors = true;
        }

        if(descr == "")
        {
            $("#presDescr").closest("div").addClass("has-error");
            hasErrors = true;
        }

        // lame but easy img 'validation'
        if(imgUrl.match(/\.(jpeg|jpg|gif|png)$/) == null)
        {
            $("#imgUrl").closest("div").addClass("has-error");
            hasErrors = true;
        }

        return hasErrors;
    }

    function clearModalInputs()
    {
        removeErrors();

        $("#presName").val("");
        $("#presDescr").val("");
        $("#imgUrl").val("");

        $("#present-preview").empty();
    }

    $('#presentCrModal').on('hidden.bs.modal', function ()
    {
        clearModalInputs();
    });

    function createPresent()
    {
        var name = $.trim($("#presName").val());
        var descr = $.trim($("#presDescr").val());
        var imgUrl = $.trim($("#imgUrl").val());

        $.ajax({

            url: "/Present/CreatePresent",
            type: "post",
            data: {
                name: name,
                descr: descr,
                imgUrl: imgUrl
            },
            success: function(response)
            {
                if(response == 0)
                {
                    $("#present-create-failed").show();
                }
                else
                {
                    $("#present-create-success").show();
                    refreshPresentContent();
                }
            }

        });

    }

    function refreshPresentContent()
    {
        $.ajax({

            url: "/Present/GetRegisteredPresentMarkup/",
            type: "post",
            success: function(response)
            {
                $("#presentWrapper").replaceWith(response);
            }
        });

    }

    function previewPresent()
    {
        var name = $.trim($("#presName").val());
        var descr = $.trim($("#presDescr").val());
        var imgUrl = $.trim($("#imgUrl").val());

        $.ajax({

            url: "/Present/GetPresentMarkup",
            type: "post",
            data: {
                op: "PREV",
                name: name,
                descr: descr,
                imgUrl: imgUrl
            },
            success: function (response)
            {
                if (response == 0)
                {
                    $("#present-preview").empty();
                    $("#present-preview").append('<div class="alert alert-danger" role="alert">Neizdevās iegūt dāvanas paraugu</div>');
                }
                else
                {
                    $("#present-preview").empty();
                    $("#present-preview").append("<h3>Dāvanas paraugs<h3>");
                    $("#present-preview").append(response);
                }
            }

        });

    }



    function removeErrors()
    {
        $(".has-error").removeClass("has-error");
        $(".alert").hide();
    }

    $("#createPresent").click(function (e)
    {
        removeErrors();

        var hasErrors = validatePresent();
        if (!hasErrors)
        {
            createPresent();
        }
    });

    $("#previewPresent").click(function (e)
    {
        removeErrors();

        var hasErrors = validatePresent();
        if (!hasErrors)
        {
            previewPresent();
        }
    });

    $("#clearInputs").click(function (e)
    {
        clearModalInputs();

    });

});