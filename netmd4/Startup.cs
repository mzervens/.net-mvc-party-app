﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(netmd4.Startup))]
namespace netmd4
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
