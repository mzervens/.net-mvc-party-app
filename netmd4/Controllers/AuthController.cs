﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using netmd4.Models;

namespace netmd4.Controllers
{
    public class AuthController : Authorization
    {
        //
        // GET: /Auth/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetLoginExistance(FormCollection collection)
        {
            UserModel um = new UserModel();
            int result = 0;
            string login = collection.Get("login").Trim();

            result = um.checkUserLoginExistance(login);

            return Content(result.ToString());
        }

        [HttpPost]
        public ActionResult TryLogin(FormCollection collection)
        {
            UserModel um = new UserModel();
            string login = collection.Get("login").Trim();
            string pass = collection.Get("password").Trim();
            string hashedPass = this.getSha256FromString(pass);

            int resultStatus = 0;

            int userId = um.login(login, hashedPass);
            if(userId != -1)
            {
                this.authorize(userId);

                if(um.isAdministrator(login))
                {
                    this.setRole(Role.ADMIN);
                }
                else
                {
                    this.setRole(Role.USER);
                }

                resultStatus = 1;
            }

            return Content(resultStatus.ToString());
        }

        [HttpPost]
        public ActionResult CreatePassword(FormCollection collection)
        {
            UserModel um = new UserModel();
            GuestModel gm = new GuestModel();
            string login = collection.Get("login").Trim();
            string pass = collection.Get("password").Trim();
            string confirmPass = collection.Get("rePassword").Trim();

            int resultStatus = 0;

            if(pass == confirmPass)
            {
                string hashedPass = this.getSha256FromString(pass);
                int guestId = gm.getGuestId(login);

                if(guestId != -1)
                {
                    int userId = um.storePassword(guestId, hashedPass);
                    this.authorize(userId);

                    if (um.isAdministrator(login))
                    {
                        this.setRole(Role.ADMIN);
                    }
                    else
                    {
                        this.setRole(Role.USER);
                    }

                    resultStatus = 1;
                }
            }

            return Content(resultStatus.ToString());

        }

        public ActionResult UnAuthorize()
        {
            this.unauthorize();
            this.clearRole();

            return RedirectToAction("Index", "Auth");
        }

        // Only used for testing purposes
        public ActionResult IsAuthorized()
        {
            int result = -1;
            if(this.isAuthorized())
            {
                result = this.getAuthorizedUserId();
            }

            return Content(result.ToString());

        }

    }
}
