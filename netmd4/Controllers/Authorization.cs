﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Text;

namespace netmd4.Controllers
{
    public enum Role
    {
        ADMIN = 0,
        USER = 1
    }


    public class Authorization : Controller
    {
        protected string userIdSessionKey = "userId";
        protected string userRoleKey = "access";

        protected void setRole(Role r)
        {
            Session[userRoleKey] = r;
        }

        protected void clearRole()
        {
            Session[userRoleKey] = null;
        }

        protected Role getRole()
        {
            var val = Session[userRoleKey];
            return (Role)val;

        }

        protected int getAuthorizedUserId()
        {
            if(isAuthorized())
            {
                return (int) Session[userIdSessionKey];
            }
            else
            {
                return -1;
            }
        }


        protected bool isAuthorized()
        {
            if (Session[userIdSessionKey] == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        protected void authorize(int userId)
        {
            Session[userIdSessionKey] = userId;
        }

        protected void unauthorize()
        {
            Session[userIdSessionKey] = null;
        }

        /*
         *  http://stackoverflow.com/questions/12416249/hashing-a-string-with-sha256 
         */
        protected string getSha256FromString(string strData)
        {
            var message = Encoding.ASCII.GetBytes(strData);
            SHA256Managed hashString = new SHA256Managed();
            string hex = "";

            var hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;
        }

	}
}