﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using netmd4.Models;

namespace netmd4.Controllers
{
    public class GuestController : Authorization
    {
        [HttpPost]
        public ActionResult CreateGuest(FormCollection collection)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            if(!this.isAuthorized())
            {
                result.Add("Error", "Unauthorized access");
                return Content(System.Web.Helpers.Json.Encode(result));
            }

            Role userRole = this.getRole();
            if(userRole != Role.ADMIN)
            {
                result.Add("Error", "You do not have permission to this opperation");
                return Content(System.Web.Helpers.Json.Encode(result));
            }

            string name = collection.Get("name").Trim();
            string surnname = collection.Get("surname").Trim();
            string email = collection.Get("email").Trim();

            GuestModel gm = new GuestModel();
            int exists = gm.getGuestId(email);

            if(exists != -1)
            {
                result.Add("Error", "Email exists");
                return Content(System.Web.Helpers.Json.Encode(result));
            }


            guestList guest = new guestList();
            guest.name = name;
            guest.surname = surnname;
            guest.email = email;

            int newGuest = gm.createGuest(guest);
            if(newGuest > 0)
            {
                result.Add("Success", newGuest.ToString());
                return Content(System.Web.Helpers.Json.Encode(result));
            }
            else
            {
                result.Add("Error", "Database error");
                return Content(System.Web.Helpers.Json.Encode(result));
            }
        }

        [HttpPost]
        public ActionResult GetGuestsMarkup()
        {
            int result = 0;
            if (!this.isAuthorized())
            {
                return Content(result.ToString());
            }

            Role userRole = this.getRole();
            if (userRole != Role.ADMIN)
            {
                return Content(result.ToString());
            }

            GuestModel gm = new GuestModel();
            List<guestList> guests = gm.getGuests();

            ViewData["guests"] = guests;

            return PartialView("RegisteredGuests");
        }

    }
}
