﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using netmd4.Models;

namespace netmd4.Controllers
{
    public class AppController : Authorization
    {

        public ActionResult Index()
        {
            if(!this.isAuthorized())
            {
                return RedirectToAction("Index", "Auth");
            }

            ViewData["userRole"] = this.getRole();

            return View();
        }


        public ActionResult GuestList()
        {
            if (!this.isAuthorized())
            {
                return RedirectToAction("Index", "Auth");
            }

            if(this.getRole() != Role.ADMIN)
            {
                return RedirectToAction("Index", "App");
            }

            GuestModel gm = new GuestModel();
            List<guestList> guests = gm.getGuests();
            ViewData["guests"] = guests;

            return View();
        }

        public ActionResult PresentList()
        {
            if (!this.isAuthorized())
            {
                return RedirectToAction("Index", "Auth");
            }


            PresentModel pm = new PresentModel();
            Role userRole = this.getRole();
            List<presents> regPres = pm.getPresents();
            int userId = this.getAuthorizedUserId();

            ViewData["userRole"] = userRole;
            ViewData["presentList"] = regPres;
            ViewData["userId"] = userId;


            return View();
        }

    }
}
