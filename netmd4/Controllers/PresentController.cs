﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using netmd4.Models;

namespace netmd4.Controllers
{
    public class PresentController : Authorization
    {
        // Metode ļauj reģistrēt / atreģistrēt dāvanu
        [HttpPost]
        public ActionResult ChoosePresent(FormCollection collection)
        {
            int result = 0;

            if(!this.isAuthorized() || this.getRole() == Role.ADMIN)
            {
                return Content(result.ToString());
            }

            PresentModel pm = new PresentModel();
            int appUserId = this.getAuthorizedUserId();

            string presentId = collection.Get("presentId").Trim();
            string operation = collection.Get("op").Trim();
            int intPresentId;

            try
            {
                intPresentId = int.Parse(presentId);
            }
            catch(Exception)
            {
                return Content(result.ToString());
            }


            List<presents> presentData = pm.getPresent(intPresentId);
            if(presentData.Count == 0)
            {
                return Content(result.ToString());
            }

            presents pres = presentData[0];

            // two supported method operations with the present
            if(operation == "CHECK")
            {
                if(pres.isTaken != 0)
                {
                    return Content(result.ToString());
                }

                pres.isTaken = 1;
                pres.takenBy = appUserId;
            }
            else if(operation == "UNCHECK")
            {
                if(pres.takenBy != appUserId)
                {
                    return Content(result.ToString());
                }

                pres.isTaken = 0;
                pres.takenBy = null;

            }
            else
            {
                return Content(result.ToString());
            }

            return Content(pm.saveModifiedPresent(pres).ToString());
        }

        [HttpPost]
        public ActionResult CreatePresent(FormCollection collection)
        {
            int result = 0;

            if(!this.isAuthorized() || this.getRole() == Role.USER)
            {
                return Content(result.ToString());
            }

            string presentName = collection.Get("name").Trim();
            string presentDescr = collection.Get("descr").Trim();
            string imgUrl = collection.Get("imgUrl").Trim();

            if(presentName == "" || presentDescr == "" || imgUrl == "")
            {
                return Content(result.ToString());
            }


            PresentModel pm = new PresentModel();
            presents pres = new presents();

            pres.name = presentName;
            pres.description = presentDescr;
            pres.url = imgUrl;
            pres.isTaken = 0;

            result = pm.createPresent(pres);

            return Content(result.ToString());

        }

        [HttpPost]
        public ActionResult GetPresentMarkup(FormCollection collection)
        {
            if (!this.isAuthorized())
            {
                return Content("0");
            }

            int presentId = 0;

            PresentModel pm = new PresentModel();

            int userId = this.getAuthorizedUserId();
            Role userRole = this.getRole();
            string operation = collection.Get("op").Trim();
            presents pres;

            if (operation == "DB")
            {
                try
                {
                    presentId = int.Parse(collection.Get("presentId").Trim());
                }
                catch (Exception)
                {
                    return Content("0");
                }

                List<presents> presentDta = pm.getPresent(presentId);

                if (presentDta.Count == 0)
                {
                    return Content("0");
                }

                pres = presentDta[0];
            }
            else
            {
                string name = collection.Get("name").Trim();
                string descr = collection.Get("descr").Trim();
                string imgUrl = collection.Get("imgUrl").Trim();

                pres = new presents();
                pres.name = name;
                pres.description = descr;
                pres.url = imgUrl;
                pres.id = -1;
                pres.isTaken = 0;
                
            }

            ViewData["present"] = pres;
            ViewData["role"] = userRole;
            ViewData["userId"] = userId;

            return PartialView("PresentPartial");

        }


        [HttpPost]
        public ActionResult GetRegisteredPresentMarkup()
        {
            if (!this.isAuthorized())
            {
                return Content("0");
            }

            PresentModel pm = new PresentModel();
            List<presents> presents = pm.getPresents();
            Role userRole = this.getRole();
            int userId = this.getAuthorizedUserId();

            ViewData["presentList"] = presents;
            ViewData["userRole"] = userRole;
            ViewData["userId"] = userId;

            return PartialView("RegisteredPresents");

        }


        [HttpPost]
        public ActionResult GetRegisteredPresentMarkupTakenBy(FormCollection collection)
        {
            if (!this.isAuthorized())
            {
                return Content("0");
            }

            int userId = this.getAuthorizedUserId();

            PresentModel pm = new PresentModel();
            List<presents> presents = pm.getPresentsTakenBy(userId);
            Role userRole = this.getRole();

            ViewData["presentList"] = presents;
            ViewData["userRole"] = userRole;
            ViewData["userId"] = userId;

            return PartialView("RegisteredPresents");

        }


    }
}
