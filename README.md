#Party app

* Lietotne tika izstrādāta kā mājas darbs LU .NET kursā.

#Palaišana

* MS SQL izveido datubāzi un importē sākotnējos datus (izmantot 4md_sql.txt).
* Web.config nomaina pieejas parametrus datubāzei

#Autorizācija
* Sistēmā sākotnēji reģistrēti 3 lietotāji (viens administrators, 2 viesi)
###Administrātora pieeja
* lietotājvārds: admin
* parole: password

###Viesis #1 
* lietotājvārds: zervens.matiss@gmail.com
* parole: Lietotājam pašam būs jāreģistrē piesakoties sistēmā

###Viesis #2
* lietotājvārds: Elina.Kalnina@lumii.lv
* parole: Lietotājam pašam būs jāreģistrē piesakoties sistēmā


#Papildus piezīmes
* Ņemt vērā, ka veidojot dāvanu, bildes url / ceļam ir jābūt kādam no šiem formātiem jpeg|jpg|gif|png


#Autors
* Matīss Zērvēns